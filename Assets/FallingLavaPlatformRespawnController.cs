﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingLavaPlatformRespawnController : RespawnController {
    FallingLavaPlatform lavaPlatform;
	// Use this for initialization
	void Start () {
        lavaPlatform = gameObject.GetComponent<FallingLavaPlatform>();
	}

    public override void OnRespawn()
    {
        base.OnRespawn();
        if(lavaPlatform != null)
        {
            lavaPlatform.Reset();
        }
    }
}
